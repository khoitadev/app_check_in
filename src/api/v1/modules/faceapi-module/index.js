const { Canvas, Image, ImageData, loadImage } = require('canvas');
const faceapi = require('face-api.js');
const path = require('path');

// Cấu hình faceapi.js để sử dụng các lớp canvas và hình ảnh từ node-canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData });

class FaceApiModule {
  faceMatcher = null;
  static async init() {
    const MODEL_PATH = path.join(__dirname, '/models');
    try {
      await Promise.all([
        faceapi.nets.ssdMobilenetv1.loadFromDisk(MODEL_PATH),
        faceapi.nets.faceLandmark68Net.loadFromDisk(MODEL_PATH),
        faceapi.nets.faceRecognitionNet.loadFromDisk(MODEL_PATH),
      ]);
      // lấy dữ liệu đã được huấn luyện
      const trainingData = await this.loadTrainingData();
      this.faceMatcher = new faceapi.FaceMatcher(trainingData, 0.5);
      console.log('Tải xong model nhận diện!');
    } catch (error) {
      console.log('error:::', error);
    }
  }
  static async loadTrainingData() {
    try {
      const labels = ['AnhTrinh', 'ConAnhTrinh'];

      const faceDescriptors = await Promise.all(
        labels.map(async (label) => {
          try {
            const descriptors = [];
            for (let i = 1; i <= 4; i++) {
              console.log('Training ', `${label}/${i}.jpg`);
              const image = await loadImage(path.join(__dirname, `/data/${label}/${i}.jpg`)); //lấy ra từng ảnh
              const detection = await faceapi
                .detectSingleFace(image)
                .withFaceLandmarks()
                .withFaceDescriptor();
              //lưu vào descriptors
              descriptors.push(detection.descriptor);
            }
            console.log(`Training xong dữ liệu của ${label} !`);
            return new faceapi.LabeledFaceDescriptors(label, descriptors);
          } catch (error) {
            console.log('================>>>: error ::: ', error);
            return null;
          }
        }),
      );
      return faceDescriptors;
    } catch (error) {
      console.log('error:::', error);
    }
  }
  static getData() {
    return {
      faceMatcher: this.faceMatcher,
      faceapi,
    };
  }
}

module.exports = FaceApiModule;
