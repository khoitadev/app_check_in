const jwt = require('jsonwebtoken');
const createError = require('http-errors');

module.exports = {
  validateToken: function ({ token, type = 'token' }) {
    try {
      let result = {};
      let optionSecret = {
        token: process.env.JWT_SECRET_USER,
        refresh: process.env.JWT_SECRET,
        admin: process.env.JWT_SECRET_ADMIN,
      };
      jwt.verify(token, optionSecret[type], async function (err, decoded) {
        if (err) {
          result = { payload: null, err };
        } else {
          result = { payload: decoded };
        }
      });

      return result;
    } catch (error) {
      console.log('error -:- ', error);
      return createError.Unauthorized(error.message);
    }
  },
  generateToken: async function ({ payload, remember = false, type = 'token' }) {
    try {
      let expiresIn = '7d';
      if (remember) expiresIn = '30d';
      if (type === 'refresh') expiresIn = '90d';
      let optionSecret = {
        token: process.env.JWT_SECRET_USER,
        refresh: process.env.JWT_SECRET,
        admin: process.env.JWT_SECRET_ADMIN,
      };

      let options = { expiresIn };
      let token = jwt.sign(payload, optionSecret[type], options);

      return token;
    } catch (error) {
      console.log('error:::', error);
      return createError.InternalServerError(error);
    }
  },
};
