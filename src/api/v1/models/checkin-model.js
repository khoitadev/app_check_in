const mongoose = require('mongoose');

const locationSchema = new mongoose.Schema({
  type: {
    type: String,
    enum: ['Point'],
    required: true,
  },
  coordinates: {
    type: [Number],
    required: true,
  },
});

const checkinSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: false,
    },
    location: { type: locationSchema, required: true },
    time: {
      type: Date,
      required: true,
    },
    status: {
      type: String,
      required: false,
      default: 'active', //close
    },
  },
  { timestamps: true },
);

checkinSchema.index({ location: '2dsphere' });

module.exports = mongoose.model('checkin', checkinSchema, 'checkin');
