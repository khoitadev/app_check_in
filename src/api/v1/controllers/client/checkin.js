const createError = require('http-errors');
const moment = require('moment');
const { loadImage } = require('canvas');
const TelegramBot = require('node-telegram-bot-api');
const CheckinModel = require('@v1/models/checkin-model');
const face = require('@v1/modules/faceapi-module/index');
require('dotenv').config();

const bot = new TelegramBot(process.env.TELEGRAM_BOT_TOKEN);

class CheckinController {
  static async checkin(req, res, next) {
    try {
      let image = req.file.path;
      let { name, longtitude, latitude } = req.body;
      const checkin = await CheckinModel.create({
        name,
        location: {
          type: 'Point',
          coordinates: [+longtitude, +latitude],
        },
        time: moment(),
      });
      let message = `Check in thành công
                    Tên: ${name}
                    Hình ảnh: ${image}`;
      let data = await bot.sendMessage(process.env.TELEGRAM_BOT_GROUP, message);

      return res.status(201).json({ checkin, send: data });
    } catch (error) {
      console.log('error -:- ', error);
      return next(createError.BadRequest(error.message));
    }
  }
  static async checkinFaceapi(req, res, next) {
    try {
      let { faceMatcher, faceapi } = face.getData();
      // nhận ảnh và convert sang dạng có thể đọc được
      const image = await loadImage(req.file.path);
      const _canvas = faceapi.createCanvasFromMedia(image);
      const size = { width: image.width, height: image.height };

      faceapi.matchDimensions(_canvas, size);
      //vẽ hình vuông phát hiện khuôn mặt
      const detection = await faceapi
        .detectSingleFace(image)
        .withFaceLandmarks()
        .withFaceDescriptor();
      console.log(
        '================>>>: KQ ::: ',
        faceMatcher.findBestMatch(detection.descriptor)?._label,
      );

      return res.status(201).json({ result: faceMatcher.findBestMatch(detection.descriptor) });
    } catch (error) {
      console.log('error -:- ', error);
      return next(createError.BadRequest(error.message));
    }
  }
}

module.exports = CheckinController;
