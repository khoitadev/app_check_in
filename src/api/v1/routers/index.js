const clientRouter = require('@v1/routers/client/index');

module.exports = function (app) {
  // Client
  app.use('/', clientRouter);

  app.use('/test', (req, res) => {
    return res.status(200).json({ message: 'test success' });
  });
};
