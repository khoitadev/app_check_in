const express = require('express');
const router = express.Router();
const CheckinController = require('@v1/controllers/client/checkin');
const { uploadCloud, upload } = require('~/plugins/upload-plugin');

router.post('/checkin', uploadCloud.single('image'), CheckinController.checkin);
router.post('/checkin-faceapi', upload.single('image'), CheckinController.checkinFaceapi);

module.exports = router;
