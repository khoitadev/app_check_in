const express = require('express');
const jwt = require('jsonwebtoken');
const path = require('path');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const createError = require('http-errors');
const db = require('~/api/database/mongo-connect');
const face = require('@v1/modules/faceapi-module/index');
const { logEvent } = require('@v1/helpers/log-helper');
const app = express();

global.__appRoot = path.resolve(__dirname);

require('dotenv').config();

const port = process.env.PORT || 9999;

app.use(
  cors({
    origin: '*',
    credentials: true,
    optionSuccessStatus: 200,
  }),
);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(helmet());
app.use(morgan(':date[clf] :method :url :status :res[content-length] - :response-time ms'));

app.use('/api', require('~/api'));
app.use(function (req, res, next) {
  next(createError.NotFound('This router does not exist.'));
});
app.use(function (err, req, res, next) {
  let status = err.status || 500;
  status === 400 &&
    logEvent(
      `user_id:${req.payload?.id} --> ${req.method}:${req.url} ${status} --> err:${err.message}`,
    );
  return res.status(status).json({
    status,
    error: err.message,
  });
});

app.listen(port, async () => {
  await db.connect();
  await face.init();
  console.log('RESTFUL API server started on: ' + port);
});
